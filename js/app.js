const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arr2 = ["1", "2", "3", "sea", "user", 23];

createList(arr2);


function createList(arr, elHtml = document.body) {
  const ul = document.createElement("ul");
  ul.classList.add("list");

  arr.map(el => {
    const li = document.createElement("li");
    li.innerText = el;
    li.classList.add("list__item");
    ul.insertAdjacentElement("beforeend", li);
  });

  elHtml.append(ul);
}